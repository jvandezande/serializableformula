﻿using AutoDependencyPropertyMarker;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Expressions
{
    /// <summary>
    /// Interaction logic for WPFFormulaBox.xaml
    /// </summary>
    public partial class WPFFormulaBox : UserControl
    {
        private SerializableFormula _Formula;
        private Stack<SolidColorBrush> _history = new Stack<SolidColorBrush>();
        private Boolean _MutePropertyChangedEvent;
        private List<Tag> _tags = new List<Tag>();

        private readonly SolidColorBrush[] BRACKETCOLORS =
        {
            Brushes.Blue,
            Brushes.Brown,
            Brushes.Green,
            Brushes.Orange,
            Brushes.Turquoise
        };

        public WPFFormulaBox()
        {
            UseBuildInErrorIndicator = true;
            InitializeComponent();
        }

        private void _Formula_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            CheckErrors();
        }

        private void ApplyColors()
        {
            try
            {
                rtbFormula.TextChanged -= rtbFormula_TextChanged;
                var textRange = new TextRange(rtbFormula.Document.ContentStart, rtbFormula.Document.ContentEnd);
                textRange.ClearAllProperties();
                _tags.Clear();
                SetBracketColors();
                ApplyErrorMarkers();
                //apply layout on text
                foreach (var t in _tags)
                {
                    var range = new TextRange(t.StartPosition, t.EndPosition);
                    range.ApplyPropertyValue(TextElement.ForegroundProperty, t.TextColor);
                    range.ApplyPropertyValue(TextElement.BackgroundProperty, t.BackGroundColor);
                    range.ApplyPropertyValue(TextElement.FontWeightProperty,
                                             t.IsBold ? FontWeights.Bold : FontWeights.Normal);
                }
            }
            finally
            {
                rtbFormula.TextChanged += rtbFormula_TextChanged;
            }
        }

        private void ApplyErrorMarkers()
        {
            if (_Formula.ExpressionError == null) return;
            var navigator = rtbFormula.Document.ContentStart;
            var allTextIndex = 0;
            var markerLength = _Formula.ExpressionErrorToken.Length;
            while (navigator.CompareTo(rtbFormula.Document.ContentEnd) < 0)
            {
                var context = navigator.GetPointerContext(LogicalDirection.Backward);
                if (context == TextPointerContext.ElementStart && navigator.Parent is Run)
                {
                    var run = (Run)navigator.Parent;
                    var text = run.Text;
                    var textIndex = 0;
                    foreach (var ch in text)
                    {
                        if ((_Formula.ExpressionErrorPosition <= allTextIndex) & (markerLength > 0))
                        {
                            SetColorOfText(run,
                                           textIndex,
                                           1,
                                           Brushes.White,
                                           Brushes.Red,
                                           false);
                            markerLength--;
                        }
                        textIndex++;
                        allTextIndex++;
                    }
                }
                navigator = navigator.GetNextContextPosition(LogicalDirection.Forward);
            }
        }

        private void CheckErrors()
        {
            if (String.IsNullOrEmpty(_Formula.ExpressionError))
            {
                vbWarning.Visibility = Visibility.Collapsed;
            }
            else if (UseBuildInErrorIndicator)
            {
                vbWarning.Visibility = Visibility.Visible;
                vbWarning.ToolTip = _Formula.ExpressionError;
            }
            else
            {
                vbWarning.Visibility = Visibility.Collapsed;
            }
            ApplyColors();
        }

        private void rtbFormula_TextChanged(object sender, TextChangedEventArgs e)
        {
            _MutePropertyChangedEvent = true;
            var textRange = new TextRange(rtbFormula.Document.ContentStart, rtbFormula.Document.ContentEnd);
            if (_Formula.CultureSpecificFormulaText == textRange.Text) return;
            _Formula.CultureSpecificFormulaText = textRange.Text;
            ApplyColors();
            _MutePropertyChangedEvent = false;
        }

        private void SetBracketColors()
        {
            var bracketIndex = -1;
            var navigator = rtbFormula.Document.ContentStart;
            while (navigator.CompareTo(rtbFormula.Document.ContentEnd) < 0)
            {
                var context = navigator.GetPointerContext(LogicalDirection.Backward);
                if (context == TextPointerContext.ElementStart && navigator.Parent is Run)
                {
                    var run = (Run)navigator.Parent;
                    var text = run.Text;
                    var textIndex = 0;
                    foreach (var ch in text)
                    {
                        if (ch == '(')
                        {
                            bracketIndex++;
                            var bracketColor = BRACKETCOLORS[bracketIndex % 5];
                            SetColorOfText(run, textIndex, 1, bracketColor, Brushes.White, true);
                            _history.Push(bracketColor);
                        }
                        else if (ch == ')')
                        {
                            if (_history.Count > 0)
                            {
                                var bracketColor = _history.Pop();
                                SetColorOfText(run, textIndex, 1, bracketColor, Brushes.White, true);
                                bracketIndex--;
                            }
                            else
                            {
                                SetColorOfText(run,
                                               textIndex,
                                               1,
                                               Brushes.Black,
                                               Brushes.Red,
                                               false);
                            }
                        }
                        textIndex++;
                    }
                }
                navigator = navigator.GetNextContextPosition(LogicalDirection.Forward);
            }
        }

        private void SetColorOfText(Run aRun,
                                    int aStart,
                                    int aLength,
                                    Brush aColor,
                                    Brush aBackColor,
                                    Boolean aBold)
        {
            var t = new Tag();
            t.StartPosition = aRun.ContentStart.GetPositionAtOffset(aStart, LogicalDirection.Forward);
            t.EndPosition = aRun.ContentStart.GetPositionAtOffset(aStart + aLength, LogicalDirection.Backward);
            t.IsBold = aBold;
            t.TextColor = aColor;
            t.BackGroundColor = aBackColor;
            _tags.Add(t);
        }

        private void WPFFormulaBox_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_MutePropertyChangedEvent) return;
            if (e.PropertyName == nameof(SerializableFormula.FormulaText))
            {
                _MutePropertyChangedEvent = true;
                rtbFormula.Document.Blocks.Clear();
                rtbFormula.Document.Blocks.Add(new Paragraph(new Run(_Formula.CultureSpecificFormulaText)));
                _MutePropertyChangedEvent = false;
            }
        }

        [AutoDependencyProperty]
        public SerializableFormula Formula
        {
            get
            {
                if (_Formula == null) _Formula = new SerializableFormula();
                return _Formula;
            }
            set
            {
                _MutePropertyChangedEvent = true;
                rtbFormula.TextChanged -= rtbFormula_TextChanged;
                if (_Formula != null)
                {
                    ((INotifyPropertyChanged)_Formula).PropertyChanged -= WPFFormulaBox_PropertyChanged;
                    _Formula.ErrorsChanged -= _Formula_ErrorsChanged;
                }
                _Formula = value;
                ((INotifyPropertyChanged)_Formula).PropertyChanged += WPFFormulaBox_PropertyChanged;
                _Formula.ErrorsChanged += _Formula_ErrorsChanged;
                rtbFormula.Document.Blocks.Clear();
                rtbFormula.Document.Blocks.Add(new Paragraph(new Run(_Formula.CultureSpecificFormulaText)));
                CheckErrors();
                _MutePropertyChangedEvent = false;
            }
        }

        public Boolean UseBuildInErrorIndicator
        { get; set; }

        new struct Tag
        {
            public TextPointer StartPosition;
            public TextPointer EndPosition;
            public Brush TextColor;
            public Brush BackGroundColor;
            public Boolean IsBold;
        }
    }
}
