﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Expressions;
using Expressions.Functions;
using fpexprpars;
using FluentAssertions;
using Newtonsoft.Json;
using System.Xml;
using System.Runtime.Serialization;
using System.IO;

namespace UnitTests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void SumTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 5);
            var b = new FormulaVariable("b", 4);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var formula = "a + b";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(9);
        }

        [Test]
        public void MultiplyTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 5);
            var b = new FormulaVariable("b", 4);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var formula = "a * b";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(20);
        }

        [Test]
        public void MultiplyFail1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 5);
            var b = new FormulaVariable("b", 4);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var formula = "a * bb";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void MultiplyFail2()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 5);
            var b = new FormulaVariable("b", 4);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var formula = "a * bb";
            // Act 
            sf.FormulaText = formula;
            // Assert
            sf.ExpressionError.Should().NotBeNullOrEmpty();
            sf.ExpressionErrorToken.Should().Be("bb");
            sf.ExpressionErrorPosition.Should().Be(4);
        }

        [Test]
        public void StringTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.String;
            var a = new FormulaVariable("a", "Hello");
            var b = new FormulaVariable("b", "World");
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var formula = "a + b";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateText();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be("HelloWorld");
        }

        [Test]
        public void BooleanTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Boolean;
            var a = new FormulaVariable("a", true);
            var b = new FormulaVariable("b", false);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var formula = "a and b";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateLogical();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(false);
        }

        [Test]
        [SetCulture("EN")]
        public void FunctionTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 4);
            var b = new FormulaVariable("b", 2);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var f = new TAverageFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "Average(a, b)";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(3);
        }

        [Test]
        [SetCulture("EN")]
        public void FunctionTest2()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var f = new TCountFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "COUNT(TRUE, FALSE)";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(1);
            // Arrange
            formula = "COUNT(TRUE, FALSE, TRUE, TRUE, FALSE)";
            sf.FormulaText = formula;
            // Act 
            res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(3);
            // Arrange
            formula = "COUNT(1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1)";
            sf.FormulaText = formula;
            // Act 
            res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(9);
            // Arrange
            formula = "COUNT((7 - 5), (3 + 2), (1 - 1), (2-4))";
            sf.FormulaText = formula;
            // Act 
            res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(2);
        }

        [Test]
        [SetCulture("EN")]
        public void CaseTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 4);
            var b = new FormulaVariable("b", 2);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var f = new TAverageFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "case(true, 10, false, 20, true, 30)";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(30);
        }

        [Test]
        [SetCulture("EN")]
        public void IfTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 4);
            var b = new FormulaVariable("b", 2);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var f = new TAverageFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "if(true, 10, 20)";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(10);
        }

        [Test]
        [SetCulture("EN")]
        public void PowerTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 4);
            var b = new FormulaVariable("b", 2);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var f = new TAverageFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "a ^ b";
            sf.FormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(16);
        }

        [Test]
        [SetCulture("fr-FR")]
        public void CultureTest1()
        {
          // Arrange
          var sf = new SerializableFormula();
          sf.FormulaResultType = TResultType.Float;
          var a = new FormulaVariable("a", 4);
          var b = new FormulaVariable("b", 2);
          sf.AvailableVariables.Add(a);
          sf.AvailableVariables.Add(b);
          var f = new TAverageFunction();
          sf.AvailableFunctions.Add(f);
          var formula = "Average(a; 2,0)";
          sf.CultureSpecificFormulaText = formula;
          // Act 
          var res = sf.ParseAndCalculateArithmetic();
          // Assert
          sf.ExpressionError.Should().BeNullOrEmpty();
          res.Should().Be(3);
        }

        [Test]
        [SetCulture("EN")]
        public void CultureTest2()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 4);
            var b = new FormulaVariable("b", 2);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var f = new TAverageFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "Average(a, 2.0)";
            sf.CultureSpecificFormulaText = formula;
            // Act 
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(3);
        }

        [Test]
        public void SerializableTest1()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 4);
            var b = new FormulaVariable("b", 2);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var f = new TAverageFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "Average(a, b)";
            sf.FormulaText = formula;           
            var output = JsonConvert.SerializeObject(sf);
            var deserializedProduct = JsonConvert.DeserializeObject<SerializableFormula>(output);
            deserializedProduct.AvailableFunctions.Add(f);
            deserializedProduct.AvailableVariables.Add(a);
            deserializedProduct.AvailableVariables.Add(b);
            // Act    
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(3);
            // Act    
            res = deserializedProduct.ParseAndCalculateArithmetic();
            // Assert
            deserializedProduct.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(3);
            deserializedProduct.FormulaText.Should().Be(formula);
            deserializedProduct.FormulaResultType.Should().Be(TResultType.Float);
        }

        [Test]
        public void SerializableTest2()
        {
            // Arrange
            var sf = new SerializableFormula();
            sf.FormulaResultType = TResultType.Float;
            var a = new FormulaVariable("a", 4);
            var b = new FormulaVariable("b", 2);
            sf.AvailableVariables.Add(a);
            sf.AvailableVariables.Add(b);
            var f = new TAverageFunction();
            sf.AvailableFunctions.Add(f);
            var formula = "Average(a, b)";
            sf.FormulaText = formula;
            DataContractSerializer ser = new DataContractSerializer(sf.GetType(), null, 2147483647, true, true, null);
            StringWriter tw = new StringWriter();
            string Result;
            try
            {
                XmlWriter xw = XmlWriter.Create(tw, new XmlWriterSettings
                {
                    Encoding = new UTF8Encoding()
                });
                try
                {
                    ser.WriteObject(xw, sf);
                    xw.Flush();
                    Result = tw.ToString();
                }
                finally
                {
                    IDisposable expr_56 = xw as IDisposable;
                    if (expr_56 != null)
                    {
                        expr_56.Dispose();
                    }
                }
            }
            finally
            {
                IDisposable expr_6A = tw as IDisposable;
                if (expr_6A != null)
                {
                    expr_6A.Dispose();
                }
            }
            DataContractSerializer ser2 = new DataContractSerializer(typeof(SerializableFormula), null, 2147483647, true, true, null);
            StringReader sr = new StringReader(Result);
            SerializableFormula deserializedProduct;
            try
            {
                XmlReader xr = XmlReader.Create(sr);
                try
                {
                    deserializedProduct = (ser2.ReadObject(xr) as SerializableFormula);
                }
                finally
                {
                    IDisposable expr_4F = xr as IDisposable;
                    if (expr_4F != null)
                    {
                        expr_4F.Dispose();
                    }
                }
            }
            finally
            {
                IDisposable expr_63 = sr as IDisposable;
                if (expr_63 != null)
                {
                    expr_63.Dispose();
                }
            }
            // Act    
            var res = sf.ParseAndCalculateArithmetic();
            // Assert
            sf.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(3);
            // Act 
            deserializedProduct.AvailableVariables.Add(a);
            deserializedProduct.AvailableVariables.Add(b);
            deserializedProduct.AvailableFunctions.Add(f);
            res = deserializedProduct.ParseAndCalculateArithmetic();
            // Assert
            deserializedProduct.ExpressionError.Should().BeNullOrEmpty();
            res.Should().Be(3);
            deserializedProduct.FormulaText.Should().Be(formula);
            deserializedProduct.FormulaResultType.Should().Be(TResultType.Float);
        }

    }
}
