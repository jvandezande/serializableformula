﻿using fpexprpars;
using PropertyChanged;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;

namespace Expressions
{
    [DataContract]
    [AddINotifyPropertyChangedInterface]
    public class SerializableFormula : INotifyDataErrorInfo, ICloneable
    {
        private ObservableCollection<IFormulaFunction> _AvailableFunctions = new ObservableCollection<IFormulaFunction>();
        private ObservableCollection<FormulaVariable> _AvailableVariables = new ObservableCollection<FormulaVariable>();
        private String _DecimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        private String _ListSeperator = CultureInfo.CurrentCulture.TextInfo.ListSeparator;
        private Boolean _NotAnInVariantCulture = (CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator != ".");
        private String _ParseError = string.Empty;
        private Int32 _ParseErrorPosition = -1;
        private String _ParseErrorToken = string.Empty;
        private TFPExpressionParser _Parser;
        private ErrorState _ErrorState = ErrorState.Unknown;
        [DataMember(Name = "FormulaText")]
        private String _FormulaText;

        public enum ErrorState
        {
            Unknown,
            NoError,
            Error
        }

        public readonly Char[] INVALIDVARIABLECHARS = { '(', ')', '+', '-', '*', '/', '=', '<', '>', ' ', ',', '.' };

        public SerializableFormula()
        {
            _AvailableVariables.CollectionChanged += _AvailableVariables_CollectionChanged;
            _AvailableFunctions.CollectionChanged += _AvailableFunctions_CollectionChanged;
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void _AvailableFunctions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RefreshFunctions();
        }

        private void _AvailableVariables_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RefreshFormulaVariables();
        }

        private Object GetObjectFromExpressionVariable(FormulaVariable aVariable)
        {
            switch (aVariable.Value.ResultType)
            {
                case TResultType.Float: return aVariable.Value.ResFloat;
                case TResultType.Boolean: return aVariable.Value.ResBoolean;
                case TResultType.String: return aVariable.Value.ResString;
                default: return null;
            }
        }

        private Type GetReturnType()
        {
            try
            {
                if (_Parser == null) _Parser = new TFPExpressionParser();
                var tResultType = _Parser.ResultType();
                switch ((int)tResultType)
                {
                    case 0:
                        return typeof(bool);

                    case 1:
                        return typeof(double);

                    case 2:
                        return typeof(string);
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        private Boolean Parse(String aFormulaText)
        {
            _ParseError = string.Empty;
            _ParseErrorToken = string.Empty;
            _ParseErrorPosition = -1;
            if (_Parser == null) _Parser = new TFPExpressionParser();
            if (String.IsNullOrEmpty(aFormulaText))
            {
                if (_ErrorState != ErrorState.NoError)
                {
                    _ErrorState = ErrorState.NoError;
                    ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(null));
                }
                return true;
            }
            try
            {
                _Parser.Expression = aFormulaText;
            }
            catch (ExpressionScannerException ex)
            {
                _ParseErrorPosition = ex.InvalidCharacterPosition;
                _ParseErrorToken = ex.InvalidToken;
                if ((_ErrorState != ErrorState.Error) && (_ParseError != ex.Message))
                {
                    _ParseError = ex.Message;
                    _ErrorState = ErrorState.Error;
                    ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(null));
                    return false;
                }
                _ParseError = ex.Message;
                return false;
            }
            catch (ExpressionParserException ex2)
            {
                if ((_ErrorState != ErrorState.Error) && (_ParseError != ex2.Message))
                {
                    _ParseError = ex2.Message;
                    _ErrorState = ErrorState.Error;
                    ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(null));
                    return false;
                }
                _ParseError = ex2.Message;
                return false;
            }
            catch (ArgumentOutOfRangeException ex3)
            {
                if ((_ErrorState != ErrorState.Error) && (_ParseError != ex3.Message))
                {
                    _ParseError = ex3.Message;
                    _ErrorState = ErrorState.Error;
                    ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(null));
                    return false;
                }
                _ParseError = ex3.Message;
                return false;
            }
            if (FormulaResultType != _Parser.ResultType())
            {
                var pe = "The expression results in a different result type then is defined";
                if ((_ErrorState != ErrorState.Error) && (_ParseError != pe))
                {
                    _ParseError = pe;
                    _ErrorState = ErrorState.Error;
                    ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(null));
                    return false;
                }
                _ParseError = pe;
                return false;
            }
            if (_ErrorState != ErrorState.NoError)
            {
                _ErrorState = ErrorState.NoError;
                ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(null));
            }
            return true;
        }

        private void RefreshFormulaVariables()
        {
            if (_Parser == null) _Parser = new TFPExpressionParser();
            _Parser.Variables.Clear();
            _Parser.Variables.AddVariableRange(AvailableVariables);
            Parse(this.FormulaText);
        }

        private void RefreshFunctions()
        {
            if (_Parser == null) _Parser = new TFPExpressionParser();
            _Parser.Functions.Clear();
            _Parser.Functions.AddFunctionRange(AvailableFunctions);
            Parse(this.FormulaText);
        }

        public IEnumerable GetErrors(string propertyName)
        {
            return (new String[] { _ParseError });
        }

        public Collection<FormulaVariable> GetUsedVariables()
        {
            var result = new Collection<FormulaVariable>();
            if (_Parser == null) _Parser = new TFPExpressionParser();
            foreach (var v in _Parser.UsedVariablesInFormula)
            {
                var v2 = AvailableVariables.FirstOrDefault(av => av.Name == v.Identifier.Name);
                if (v2 != null) result.Add(v2);
            }
            return result;
        }

        public double ParseAndCalculateArithmetic()
        {
            if (FormulaResultType != TResultType.Float)
            {
                throw new SerializableFormulaException("Formula is not supposed to be Arithmetic");
            }
            if (!Parse(this.FormulaText))
            {
                return double.NaN;
            }
            if (GetReturnType() == typeof(double))
            {
                var res = _Parser.Evaluate();
                return res.ResFloat;
            }
            throw new SerializableFormulaException("Expression was not Arithmetic");
        }

        public Boolean? ParseAndCalculateLogical()
        {
            if (FormulaResultType != TResultType.Boolean)
            {
                throw new SerializableFormulaException("Formula is not supposed to be Logical");
            }
            if (!Parse(this.FormulaText))
            {
                return null;
            }
            if (GetReturnType() == typeof(Boolean))
            {
                var res = _Parser.Evaluate();
                return res.ResBoolean;
            }
            throw new SerializableFormulaException("Expression was not Logic");
        }

        public String ParseAndCalculateText()
        {
            if (FormulaResultType != TResultType.String)
            {
                throw new SerializableFormulaException("Formula is not supposed to be Text");
            }
            if (!Parse(this.FormulaText))
            {
                return null;
            }
            if (GetReturnType() == typeof(String))
            {
                var res = _Parser.Evaluate();
                return res.ResString;
            }
            throw new SerializableFormulaException("Expression was not Text");
        }

        public SerializableFormula Clone()
        {
            var clone = new SerializableFormula();
            clone.FormulaText = this.FormulaText;
            clone.FormulaResultType = this.FormulaResultType;
            return clone;
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public ObservableCollection<IFormulaFunction> AvailableFunctions
        {
            get
            {
                if (_AvailableFunctions == null)
                {
                    _AvailableFunctions = new ObservableCollection<IFormulaFunction>();
                    _AvailableFunctions.CollectionChanged += _AvailableFunctions_CollectionChanged;
                }
                return _AvailableFunctions;
            }
            set
            {
                if (_AvailableFunctions != null)
                {
                    _AvailableFunctions.CollectionChanged -= _AvailableFunctions_CollectionChanged;
                }
                _AvailableFunctions = value;
                RefreshFunctions();
                _AvailableFunctions.CollectionChanged += _AvailableFunctions_CollectionChanged;
            }
        }

        public ObservableCollection<FormulaVariable> AvailableVariables
        {
            get
            {
                if(_AvailableVariables == null)
                {
                    _AvailableVariables = new ObservableCollection<FormulaVariable>();
                    _AvailableVariables.CollectionChanged += _AvailableVariables_CollectionChanged;
                }
                return _AvailableVariables;
            }
            set
            {
                if (_AvailableVariables != null)
                {
                    _AvailableVariables.CollectionChanged -= _AvailableVariables_CollectionChanged;
                }
                _AvailableVariables = value;
                RefreshFormulaVariables();
                _AvailableVariables.CollectionChanged += _AvailableVariables_CollectionChanged;
            }
        }

        public String CultureSpecificFormulaText
        {
            get
            {
                var result = this.FormulaText;
                if (result == null) return String.Empty;
                if (String.IsNullOrEmpty(_ListSeperator))
                {
                    _ListSeperator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    _DecimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    _NotAnInVariantCulture = (_DecimalSeparator != ".");
                }
                if (_NotAnInVariantCulture)
                {
                    result = result.Replace(",", _ListSeperator);
                    result = result.Replace(".", _DecimalSeparator);
                }
                return (result);
            }

            set
            {
                if (String.IsNullOrEmpty(_ListSeperator))
                {
                    _ListSeperator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
                    _DecimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    _NotAnInVariantCulture = (_DecimalSeparator != ".");
                }
                if (_NotAnInVariantCulture)
                {
                    value = value.Replace(_DecimalSeparator, ".");
                    value = value.Replace(_ListSeperator, ",");
                }
                this.FormulaText = value;
            }
        }

        public String ExpressionError
        {
            get
            {
                return (_ParseError);
            }
        }

        public Int32 ExpressionErrorPosition
        {
            get
            {
                return (_ParseErrorPosition);
            }
        }

        public String ExpressionErrorToken
        {
            get
            {
                return (_ParseErrorToken);
            }
        }

        [DataMember]
        public TResultType FormulaResultType
        {
            get; set;
        }

        public String FormulaText
        {
            get
            {
                if (_Parser == null) return _FormulaText;
                if (String.IsNullOrEmpty(_Parser.Expression) & !String.IsNullOrEmpty(_FormulaText)) return _FormulaText;
                return _Parser.Expression;
            }
            set
            {
                _FormulaText = value;
                Parse(value);
            }
        }

        public bool HasErrors
        {
            get
            {
                return (!String.IsNullOrEmpty(_ParseError));
            }
        }
    }

    public class SerializableFormulaException : Exception
    {
        public SerializableFormulaException(String aMessage) : base(aMessage)
        {
        }
    }
}
